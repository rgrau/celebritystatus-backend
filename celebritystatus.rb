#shotgun app.rb -p 9294
require 'sinatra'
require "json"
require 'httpclient'
require 'pry'

def is_dead(name)
  json = HTTPClient.get("http://en.wikipedia.org/w/api.php?action=query&titles=#{name}&prop=revisions&rvprop=content&format=json&rvsection=0")
  lines = json.body.split("\\n")
  is_dead = lines.grep(/death_date/)
  if is_dead.size > 0
    return true
  else
    return false
  end
end

get '/is_dead' do
  puts request.query_string
  name = params[:name]
  {name: params[:name],
   is_dead: is_dead(name)}.to_json
end
